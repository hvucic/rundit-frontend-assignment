import CompanyInterface from "./company.interface";

type DateString = string; // YYYY-MM-DD

interface Investment {
  amount: number;
  companyId: number;
  date: DateString;
  id: number;
}

interface InvestmentExtended extends Investment {
  company: CompanyInterface;
}

export { Investment, InvestmentExtended };
