import { format, parseISO } from "date-fns";
import Vue from "vue";

import App from "./App.vue";
import store from "./store";
import router from "./router";

const DATE_FORMAT = "dd.MM.y";

Vue.filter("dateFormat", (date: string): string =>
  format(parseISO(date), DATE_FORMAT)
);
Vue.filter("numberFormat", (number: number): string =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
);
Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
