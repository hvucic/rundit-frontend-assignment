import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

import Summary from "../summary/index.vue";
import Investments from "../investments/index.vue";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "Investments",
    component: Investments,
  },
  {
    path: "/summary",
    name: "Summary",
    component: Summary,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
