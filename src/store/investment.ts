import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import CompanyInterface from "@/interfaces/company.interface";
import CompanyService from "@/services/company.service";
import { InvestmentExtended as InvestmentInterface } from "@/interfaces/investment.interface";
import InvestmentService from "@/services/investment.service";

@Module({ namespaced: true })
class Investment extends VuexModule {
  public investments: InvestmentInterface[] = [];
  public companies: CompanyInterface[] = [];
  public isLoading = false;
  public error: Error | null = null;

  get investmentsByCompanyId(): { [key: number]: InvestmentInterface[] } {
    return this.investments.reduce((acc, investment) => {
      if (!acc[investment.companyId]) {
        acc[investment.companyId] = [];
      }

      acc[investment.companyId].push(investment);

      return acc;
    }, {} as { [key: number]: InvestmentInterface[] });
  }

  @Action({ rawError: true })
  public async getAll(): Promise<void> {
    this.context.commit("setIsLoading", true);

    try {
      const [investments, companies] = await Promise.all([
        InvestmentService.getAll(),
        CompanyService.getAll(),
      ]);
      const companiesById: Map<number, CompanyInterface> = companies.reduce(
        (acc, company) => acc.set(company.id, company),
        new Map()
      );
      const updatedInvestments: InvestmentInterface[] = investments.map(
        (investment) => ({
          ...investment,
          /**
           * Typescript throws "Type 'undefined' is not assignable to type 'Company'" although it will not be null
           * See issue here: https://github.com/microsoft/TypeScript/issues/9619
           */
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          company: companiesById.get(investment.companyId)!,
        })
      );

      this.context.commit("setAll", updatedInvestments);
      this.context.commit("setCompanies", companies);
    } catch (err) {
      this.context.commit("setError", err);
    } finally {
      this.context.commit("setIsLoading", false);
    }
  }

  @Action({ rawError: true })
  public async remove(id: number): Promise<void> {
    try {
      await InvestmentService.deleteById(id);

      this.context.commit("removeFromState", id);
    } catch (err) {
      this.context.commit("setError", err);
    }
  }

  @Mutation
  public setAll(investments: InvestmentInterface[]): void {
    this.investments = investments;
  }

  @Mutation
  public setCompanies(companies: CompanyInterface[]): void {
    this.companies = companies;
  }

  @Mutation
  public setIsLoading(isLoading: boolean): void {
    this.isLoading = isLoading;
  }

  @Mutation
  public setError(error: Error | null = null): void {
    this.error = error;
  }

  @Mutation
  public removeFromState(id: number): void {
    this.investments = this.investments.filter((i) => i.id !== id);
  }
}

export default Investment;
